package com.example.ankit.myapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.ankit.myapplication.Api.FotoniciaService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.textView);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dev.fotonicia.in/rest/default/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final FotoniciaService service = retrofit.create(FotoniciaService.class);

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                Response<Boolean> res = null;
                try {
                    Map<String, String> data = new HashMap<String, String>();
//                    data.put("phone", "9717271227");
                    Call<Boolean> request = service.isPhoneAvailable(data);
                    res = request.execute();
                    if (res.isSuccessful()) {
                        return "Is phone available: " + res.body();
                    } else {
                        if (res.errorBody() != null) {
                            return res.errorBody().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "JSON exception";
            }

            @Override
            protected void onPostExecute(String data) {
                mTextView.setText(data);
            }
        }.execute();


    }
}
