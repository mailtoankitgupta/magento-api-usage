package com.example.ankit.myapplication.Api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ankit on 26/12/17.
 */

public interface FotoniciaService {
    @POST("V1/customers/isPhoneAvailable")
    Call<Boolean> isPhoneAvailable(@Body Map<String, String> phone);
}